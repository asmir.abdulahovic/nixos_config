# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./fw.nix
    ];


  system.stateVersion = "21.03";

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader.grub.device = "/dev/sda";
    loader.grub.enable = true;
    loader.grub.version = 2;
    tmpOnTmpfs = true;
  };

  networking = {
    firewall.enable = false;
    hostName = "nixy";
    interfaces.enp0s25.useDHCP = true;
    interfaces.wlp3s0.useDHCP = true;
    useDHCP = false;
    wireless.enable = true;
  };

  time.timeZone = "Europe/Sarajevo";

  nixpkgs.config.allowUnfree = true;
  environment = {
    homeBinInPath = true;
    variables = {
      PATH = "$HOME/.cargo/bin";
    };
    systemPackages = with pkgs; [
      acpi
      amfora
      axel
      bemenu
      bluez
      bluez-tools
      ccls
      clang
      clang-analyzer
      clang-tools
      cmake
      direnv
      fd
      ffmpeg
      file
      firefox
      foot
      fzf
      fzy
      gammastep
      gcc
      gcc-arm-embedded
      gdb
      gnumake
      graphviz
      htop
      imv
      inkscape
      interception-tools
      jq
      kodi
      macchanger
      meson
      moreutils
      mpc_cli
      mpv
      nano
      neovim
      nix-index
      nodejs
      nox
      ntfs3g
      ntfsprogs
      pass
      perl
      polybar
      pstree
      psutils
      pulseaudio-ctl
      pulsemixer
      python3
      python3Packages.pip
      qemu
      qutebrowser
      remmina
      ripgrep
      rnix-lsp
      rsync
      silver-searcher
      strace
      thunderbird
      tig
      unrar
      unzip
      usbutils
      weechat
      wget
      wireshark
      wl-clipboard
      xdg_utils
      xorg.xhost
      xsel
      zathura
    ];
  };

  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.zsh.enable = true;
  programs.light.enable = true;
  programs.firejail.enable = true;

  # List services that you want to enable:
  systemd = {
    services = {
      "macchanger-wireless" = {
        after = [ "sys-subsystem-net-devices-wlp3s0.device" ];
        before = [ "network-pre.target" ];
        bindsTo = [ "sys-subsystem-net-devices-wlp3s0.device" ];
        description = "Changes MAC of my wireless interface for privacy reasons";
        stopIfChanged = false;
        wantedBy = [ "multi-user.target" ];
        wants = [ "network-pre.target" ];
        script = ''
          ${pkgs.macchanger}/bin/macchanger -e wlp3s0
        '';
        serviceConfig.Type = "oneshot";
      };

      "caps2esc" = {
        description = "Intercepts keyboard udev events";
        wants = [ "systemd-udevd.service" ];
        wantedBy = [ "multi-user.target" ];
        script = ''
          ${pkgs.interception-tools}/bin/intercept \
            -g /dev/input/by-path/platform-i8042-serio-0-event-kbd | \
            /opt/caps2esc | ${pkgs.interception-tools}/bin/uinput \
            -d /dev/input/by-path/platform-i8042-serio-0-event-kbd
        '';
        serviceConfig.Type = "simple";
      };
    };
  };
  services = {
    fwupd.enable = true;
    fstrim.enable = true;
    ntp.enable = false;
    openssh.enable = true;
    printing.enable = true;
    thinkfan.enable = true;
    tlp.enable = true;
    acpid.enable = true;

    udev.packages = [ pkgs.openocd pkgs.rtl-sdr ];


    xserver = {
      enable = true;
      autoRepeatDelay = 200;
      autoRepeatInterval = 17;
      desktopManager.xterm.enable = false;
      displayManager.defaultSession = "none+i3";
      layout = "us";
      windowManager.i3 = {
        enable = true;
      };
      libinput = {
        enable = true;
        accelProfile = "flat";
        additionalOptions = ''
          Option "AccelSpeed" "1"
        '';
      };
    };

    actkbd = {
      enable = true;
      bindings = [
        { keys = [ 121 ]; events = [ "key" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master toggle"; }
        { keys = [ 122 ]; events = [ "key" "rep" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master ${config.sound.mediaKeys.volumeStep}- unmute"; }
        { keys = [ 123 ]; events = [ "key" "rep" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master ${config.sound.mediaKeys.volumeStep}+ unmute"; }
        { keys = [ 224 ]; events = [ "key" ]; command = "/run/current-system/sw/bin/light -U 10"; }
        { keys = [ 225 ]; events = [ "key" ]; command = "/run/current-system/sw/bin/light -A 10"; }
      ];
    };

    mpd = {
      musicDirectory = "/home/mpd/music";
      enable = true;
      extraConfig = ''
        audio_output {
          type "pulse"
          name "pulsee srv"
          server "127.0.0.1"
        }
      '';
    };
  };

  fonts.fonts = with pkgs; [
    dina-font
    fira-code
    fira-code-symbols
    font-awesome
    font-awesome_4
    font-awesome-ttf
    iosevka
    jetbrains-mono
    liberation_ttf
    mplus-outline-fonts
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    proggyfonts
    siji
  ];

  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };
  };

  sound.enable = true;

  hardware = {
    bluetooth.enable = true;

    pulseaudio = {
      enable = true;
      systemWide = true;
      tcp = {
        enable = true;
        anonymousClients.allowedIpRanges = [ "127.0.0.1" ];
      };
    };

    opengl = {
      enable = true;
      driSupport = true;
    };
  };

  zramSwap = {
    enable = true;
    algorithm = "zstd";
  };

  users.users.akill = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "kvm" "tty" "audio" ];
  };
}
